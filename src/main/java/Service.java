import com.google.gson.Gson;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Node;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.port;
import static spark.Spark.post;

/**
 * Created by node-iboy on 17/05/17.
 */
public class Service {

    private static final String payment="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ws.wso2.org/dataservice\">\n" +
            "   <SOAP-ENV:Body>\n" +
            "      <ns1:va_checkbalance >\n" +
            "         <ns1:ID_PETUGAS>%s</ns1:ID_PETUGAS>\n" +
            "         <ns1:PASS_PETUGAS>%s</ns1:PASS_PETUGAS>\n" +
            "         <ns1:IDTIPEREFNOMOR>%s</ns1:IDTIPEREFNOMOR>\n" +
            "         <ns1:DATA>%s|</ns1:DATA>\n" +
            "         <ns1:SIGNATURE>%s</ns1:SIGNATURE>\n" +
            "      </ns1:va_checkbalance >\n" +
            "   </SOAP-ENV:Body>\n" +
            "</SOAP-ENV:Envelope>";

    public static void main(String[] args){
        RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String, String> syncCommands = connection.sync();
        port(7010);
        System.out.print("Start spark with port: 7010");

        post("/checkbalance", (req, res) -> {
            Map<String, String> map;
            String status="";
            try{
                map = new Gson().fromJson(req.body(), Map.class);
            }catch (Exception e){
                res.status(400);
                return "{bad request}";
            }
            try {
                String data ="%s|";
                String dataFormatted = String.format(data,String.valueOf(map.get("account")));
                String signature=String.valueOf(map.get("idPetugas")).concat(String.valueOf(map.get("passPetugas"))).concat(String.valueOf(map.get("idTiPeref")));
                String body = String.format(payment,String.valueOf(map.get("idPetugas")),String.valueOf(map.get("passPetugas")),String.valueOf(map.get("idTiPeref")),
                        String.valueOf(map.get("account")),
                        sha1converter(signature.concat(dataFormatted)));
                HttpResponse<String> jsonResponse = Unirest.post(String.valueOf(map.get("url")).concat("/services/VAPOSService.SOAP12Endpoint/"))
                        .body(body).asString();
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(jsonResponse.getBody())));
                org.w3c.dom.Node n = document.getFirstChild();
                NodeList nl = n.getChildNodes();
                org.w3c.dom.Node an,an2,an3;

                Map<String,String>  tagNode = new HashMap<>();
                for (int i=0; i < nl.getLength(); i++) {
                    an = nl.item(i);
                    if(an.getNodeType()== Node.ELEMENT_NODE) {
                        NodeList nl2 = an.getChildNodes();
                        for(int i2=0; i2<nl2.getLength(); i2++) {
                            an2 = nl2.item(i2);
                            NodeList nl3 = an2.getChildNodes();
                            for(int i3=0; i3<nl3.getLength();i3++){
                                an3 = nl3.item(i3);
                                org.w3c.dom.Node sibling = an3.getFirstChild();
                                //input first child node
                                if(sibling!=null)tagNode.put(sibling.getNodeName()+i3,sibling.getTextContent());
                                while (sibling != null) {
                                    try{
                                        //iterate next child
                                        sibling = sibling.getNextSibling();
                                        tagNode.put( sibling.getNodeName()+i3,sibling.getTextContent());
                                    }catch (NullPointerException e){
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
                String result = "{";
                for(int i=0; i<tagNode.size()/3;i++){
                    if(i>0)result =result+",";
                    if(!tagNode.get("RESPONSE"+i).equals("DATA DITEMUKAN")){
                        res.status(400);
                        return "{Account Not Found}";
                    }
                    result = result+tagNode.get("ID_RESPONSE"+i)+":"+tagNode.get("RESPONSE"+i)+":"+tagNode.get("DATA"+i);
                }
                result = result+"}";

                res.status(200);
                return result;

                /*return body;*/
            } catch (Exception e) {
                res.status(401);
                return "{Check Balance Error}";
            }
        });
    }

    private static String sha1converter(String param){
        return DigestUtils.sha1Hex(param);
    }
}
